#define trigPin 8
#define echoPin 9
#define buzzer 10

long duration;
float distanceCm;
int timer;

void setup() {
pinMode(trigPin, OUTPUT); 
pinMode(echoPin, INPUT); 
pinMode(buzzer, OUTPUT);
}

void loop() {
digitalWrite(trigPin, LOW);
delayMicroseconds(2);
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);

duration = pulseIn(echoPin, HIGH);

distanceCm= duration*0.034/2;

digitalWrite(buzzer, HIGH);
delay(50);
digitalWrite(buzzer, LOW);

timer = distanceCm * 10;

delay(timer);
}
