#include <LiquidCrystal.h>

int battVoltPin = 1;                //Assign battery voltage to pin 1
int solVoltPin = 2;                 //Assign solar panel voltage to pin 2
int battCurrentPin = 3;             //Assign battery current sensor to pin 3
int solCurrentPin = 4;              //Assign solar panel current sensor to pin 4
int greenLEDPin = 1;                //Assign green LED to pin 1
int redLEDPin = 2;                  //Assign red LED to pin 2
int alarmPin = 3;                   //Assign alarm to pin 3
double batterykWH = 0;
double solarkWH = 0;
int battCapacity = 105;             //The battery's usable capacity in Amp Hours

unsigned long startMillis;          //Variables to track time for each cycle
unsigned long endMillis;

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);  //Assign LCD screen pins, as per LCD shield requirements

void setup() 
{ 
  pinMode(greenLEDPin, OUTPUT);  //Define the pin functions
  pinMode(redLEDPin, OUTPUT);
  pinMode(alarmPin, OUTPUT);
  lcd.begin(16,2);              //Start the LCD, columns, rows.  use 16,2 for a 16x2 LCD, etc.
  lcd.clear();
  lcd.setCursor(0,0);           //Set cursor to column 0, row 0 (the first row)
  lcd.print("Van Life");
  lcd.setCursor(0,1);           //Set cursor to column 0, row 1 (the second row)
  lcd.print("Power Info");
  startMillis = millis();
}

void loop() 
{ 
  double battVoltage = 0;
  double battCurrent = 0;
  battVoltage = 15*analogRead(battVoltPin)/1023;            //Read the battery voltage
  battCurrent = 30*(analogRead(battCurrentPin)-511)/512;    //Read the battery current
  double battPower = battVoltage*battCurrent;
  double solarVoltage = 0;
  double solarCurrent = 0;
  solarVoltage = 15*analogRead(solVoltPin)/1023;            //Read the solar panel voltage
  solarCurrent = 30*(analogRead(solCurrentPin)-511)/512;    //Read the solar panel current
  double solPower = solarVoltage*solarCurrent;
  if(solPower>=battPower)                                   //If solar power is greater than power being drawn then battery is charging
  {
    digitalWrite(greenLEDPin, HIGH);
  }
  else
  {
    digitalWrite(greenLEDPin, LOW);
  }
  if(battVoltage<=12.10)                                    //Low voltage indication
  {
    digitalWrite(redLEDPin, HIGH);
    digitalWrite(alarmPin, LOW);
  }
  else if (battVoltage<=11.95)                              //Low voltage alarm
  {
    digitalWrite(redLEDPin, HIGH);
    digitalWrite(alarmPin, HIGH);
  }
  else
  {
    digitalWrite(redLEDPin, LOW);
    digitalWrite(alarmPin, LOW);
  }
  double battPercentage = 100*((battVoltage-11.9)/(12.7-11.9));  //Calculate battery percentage remaining
  double battTime = (battCapacity/battCurrent);               //Calculate battery time remaining
  endMillis = millis();
  unsigned long time = endMillis - startMillis;
  batteryKWH = batteryKWH + (battPower * (time/60/60/1000000));  //Calculate battery kWh used since start
  solarKWH = solarKWH + (solPower * (time/60/60/1000000));       //Calculate solar panel kWh generated since start
  startMillis = millis();
  delay (4000);
  lcd.clear();                  // Start updating display with new data
  lcd.setCursor(0,0);
  lcd.print("Battery");
  delay (1000);
  lcd.clear();
  lcd.setCursor(0,0);           // Displays all battery data
  lcd.print(battVoltage);
  lcd.print("V");
  lcd.setCursor(9,0);
  lcd.print(battCurrent);
  lcd.print("A");
  lcd.setCursor(0,1);
  lcd.print(battPower);
  lcd.print("W");
  lcd.setCursor(9,1);
  lcd.print(batteryKWH);
  lcd.print("kWh");
  delay (4000);
  lcd.clear();
  lcd.setCursor(0,0);           // Displays all battery capacity data
  if(solPower>=battPower)
  {
    lcd.print("Battery");
    lcd.setCursor(0,1);
    lcd.print("Charging");
  }
  else
  {
    lcd.print("Remaining ");
    lcd.print(battPercentage);
    lcd.print("%");
    lcd.setCursor(0,1);
    lcd.print("Time ");
    lcd.print(battTime);
    lcd.print("hrs");
  }
  delay (4000);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Solar Panel");
  delay (1000);
  lcd.clear();
  lcd.setCursor(0,0);           // Displays all solar panel data
  lcd.print(solarVoltage);
  lcd.print("V");
  lcd.setCursor(9,0);
  lcd.print(solarCurrent);
  lcd.print("A");
  lcd.setCursor(0,1);
  lcd.print(solPower);
  lcd.print("W");
  lcd.setCursor(9,1);
  lcd.print(solarKWH);
  lcd.print("kWh");
}
