#include <Adafruit_Sensor.h>
#include <DHT.h>
int speakerPin = 9;
int length = 1;
#define DHTPIN 5
#define DHTTYPE DHT11
#define LED_TOO_COLD A0
#define LED_PERFECT A1
#define LED_TOO_HOT A2
DHT dht(DHTPIN, DHTTYPE);
void setup() {
  Serial.begin(9600);
  Serial.println("DHT11 test!");

  dht.begin();
}

void loop() {
  pinMode(speakerPin, OUTPUT);
  pinMode (A0 , OUTPUT);
  pinMode (A1 , OUTPUT);
  pinMode (A2 , OUTPUT);
  delay(500);

  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float f = dht.readTemperature(true);


  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.println(" *C ");
  if (t <= 19) {
    Serial.println("Too cold!");
    digitalWrite(A0, HIGH);
    digitalWrite(speakerPin, HIGH);
    delay (500);
    digitalWrite(speakerPin, LOW);
    digitalWrite(A0, LOW);
  }
  if (20 < t < 27) {
    Serial.println("Perfect temperature!");
    digitalWrite(A1, HIGH);
  
  }
  if (t >= 28) {
    Serial.println("Too hot!");
    digitalWrite(A2, HIGH);
    digitalWrite(speakerPin, HIGH);
    delay (500);
    digitalWrite(speakerPin, LOW);
    digitalWrite(A2, LOW);
  }

    if (h <= 30) {
    Serial.println("Humidity too low!");
    digitalWrite(A3, HIGH);
    digitalWrite(speakerPin, HIGH);
    delay (500);
    digitalWrite(speakerPin, LOW);
    digitalWrite(A3, LOW);
  }
    if (31 < h < 70) {
    Serial.println("Humidity perfect!");
    digitalWrite(A4, HIGH);
    digitalWrite(speakerPin, HIGH);
  
  }

    if (h >= 71) {
    Serial.println("Too high humidity!");
    digitalWrite(A5, HIGH);
    digitalWrite(speakerPin, HIGH);
    delay (500);
    digitalWrite(speakerPin, LOW);
    digitalWrite(A5, LOW);
  }
}
